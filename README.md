Os Maven Goals são tarefas específicas que contribuem para a criação, contrução e execução de um projeto. Só quando todos os Goals são atingidos é que o processo ou a fase fica completos.

# Docker cheatsheet 

## List Docker CLI commands
docker
docker container --help

## Display Docker version and info
docker --version
docker version
docker info

## Execute Docker image
docker run hello-world

## List Docker images
docker image ls

## List Docker containers (running, all, all in quiet mode)
docker container ls
docker container ls --all
docker container ls -aq
