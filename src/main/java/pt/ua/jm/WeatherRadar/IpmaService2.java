package pt.ua.jm.WeatherRadar;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IpmaService2 {
    @GET("distrits-islands.json")
    Call<CitiesList> getCities();
}