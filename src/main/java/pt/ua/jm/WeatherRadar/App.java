package pt.ua.jm.WeatherRadar;

import retrofit2.Retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://api.ipma.pt/open-data/")
            .addConverterFactory(GsonConverterFactory. create ())
            .build();

        IpmaService2 service1 = retrofit.create(IpmaService2.class);

        Call<CitiesList> callSync1 = service1.getCities();

        String CITY_NAME = "";
        int CITY_ID= 0;

        if(args.length > 0) {
            CITY_NAME = args[0];
        }

        try {
            // System.out.println(callSync1.execute());
            Response<CitiesList> apiResponse = callSync1.execute();
            CitiesList citiesList = apiResponse.body();
            // System. out .println( "max temp for today: " + cities_list.getData().
            // listIterator().next().getTMax());
            CITY_ID = citiesList.getCityId(CITY_NAME);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println(CITY_ID);

        IpmaService service = retrofit.create(IpmaService.class);

        Call<IpmaCityForecast> callSync = service.getForecastForACity( CITY_ID );

        try {
            Response<IpmaCityForecast> apiResponse = callSync.execute();
            IpmaCityForecast forecast = apiResponse.body();
            System. out .println( "max temp for today: " + forecast.getData().
            listIterator().next().getTMax());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
